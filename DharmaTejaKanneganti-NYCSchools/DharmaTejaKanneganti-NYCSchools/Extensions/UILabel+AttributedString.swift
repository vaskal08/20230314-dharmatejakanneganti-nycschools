//
//  UILabel+AttributedString.swift
//  DharmaTejaKanneganti-NYCSchools
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import UIKit

extension UILabel {
    
    /**
        - Parameters:
        - withString string: Actual string
        - boldString: bold string
        - font: Actual font
     */
    
    func attributedText(withString string : String, boldString : String) {
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: (self.font ?? UIFont.systemFont(ofSize: 17))])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: self.font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        self.attributedText = attributedString
    }
}
