//
//  Notifications.swift
//  DharmaTejaKanneganti-NYCSchools
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import Foundation

extension Notification.Name {
    static let flagsChanged = Notification.Name("FlagsChanged")
}
