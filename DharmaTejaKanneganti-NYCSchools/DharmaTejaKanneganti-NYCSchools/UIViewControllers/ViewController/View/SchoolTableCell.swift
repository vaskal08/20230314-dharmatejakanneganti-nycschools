//
//  SchoolTableCell.swift
//  DharmaTejaKanneganti-NYCSchools
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import UIKit

class SchoolTableCell: UITableViewCell {

    //MARK: - outlets
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    var school: SchoolModel! {
        didSet {
            schoolNameLabel.text = school.schoolName
            if let city = school.city, let code = school.stateCode, let zip = school.zip{
                cityLabel.text = "\(city), \(code), \(zip)"
            }
        }
    }
    //MARK: - awakeFromNib
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK: - setSelected
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
