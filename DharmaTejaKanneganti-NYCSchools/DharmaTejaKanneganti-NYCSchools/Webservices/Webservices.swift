//
//  Webservices.swift
//  DharmaTejaKanneganti-NYCSchools
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import Foundation

class Webservices
{
    init() {}
    var baseWebservice : BaseWebservice = BaseWebservice.init()
    
    /// This method is used to fetch all schools data from API using getSchools end point

    func fetchSchoolsDataFromAPI(completion: @escaping ((SchoolsData?, Error?) -> Void)) {
        let urlString = Constants.baseURL + Constants.gethSchools
        guard let url = URL(string: urlString) else {
            completion(nil, baseWebservice.apiError)
            return
        }
        baseWebservice.executeAPIRequest(url: url) { responseData, error in
            guard let responseData = responseData else { return completion(nil, error) }
            Utilites.convertResponseToModel(type: SchoolsData.self, from: responseData, completion: completion)
        }
    }
    
    // This method is used to fetch all schools SAT data from API using getSchools end point
    
    func fetchSchoolsSATDataFromAPI(completion: @escaping ((SchoolsSATData?, Error?) -> Void)) {
        let urlString = Constants.baseURL + Constants.getSATScores
        guard let url = URL(string: urlString) else {
            completion(nil, baseWebservice.apiError)
            return
        }
        baseWebservice.executeAPIRequest(url: url) { responseData, error in
            guard let responseData = responseData else { return completion(nil, error) }
            Utilites.convertResponseToModel(type: SchoolsSATData.self, from: responseData, completion: completion)
        }
    }
}
