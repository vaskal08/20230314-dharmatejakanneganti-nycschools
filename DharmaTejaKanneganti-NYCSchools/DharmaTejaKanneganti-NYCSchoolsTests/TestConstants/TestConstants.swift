//
//  TestConstants.swift
//  DharmaTejaKanneganti-NYCSchoolsTests
//
//  Created by Dharma Teja Kanneganti on 14/03/23.
//

import Foundation

let kAPIRequestWaitTime = TimeInterval(10)

let schoolsAPIPath = "/resource/s3k6-pzi2.json"
let schoolsAPIResponse = "schools.json"

let schoolsSatAPIPath = "/resource/f9bf-2cp4.json"
let satAPIResponse = "schoolSATResponse.json"
